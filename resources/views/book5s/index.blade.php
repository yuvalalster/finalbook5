

@yield('content')

@extends('layouts.app')
@section('content')

<h1> This is your book list</h1>

<table>
    <tr>
    <th>Book Name</th>
    <th>Author</th>
    <th>status</th>
    </tr>
    @foreach($book5s as $book5s)
    <tr>
    <td>@can('manager')<a href="{{route('book5s.edit',$book5s->id)}}">@endcan {{$book5s->title}} </a></td>
    <td>{{$book5s->author}}</td>
    <td>  @if ($book5s->status)
        <input type = 'checkbox' id ="{{$book5s->id}}" checked>
        <!-- <input type = 'checkbox' id ="{{$book5s->id}}" disabled='disable' checked>-->
       @else
           <input type = 'checkbox' id ="{{$book5s->id}}">
       @endif </td>
    </tr>
   

    @endforeach
    </table>

   @can('manager') <a href = "{{route('book5s.create')}}"> Create a new book</a>@endcan

    <script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
            //$(this).attr('disabled', true);
           // console.log(event.target.id)
               $.ajax({
                   url: "{{url('book5s')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType: 'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>  



    <script>
    /*     $(document).ready(function(){
           $(":chekbox").click(function(event){
                console.log(event.target.id)
                $.ajax({
                   url: "{{url('books')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType:'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>
     

    <style>
table, th, td {
  border: 1px solid black;
}
    </style>
@endsection